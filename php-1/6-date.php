<?

  /**
   * PHP bietet eine sehr elegante Methode mit Daten zu arbeiten
   */

  // Aktuelles Datum und Uhrzeit
  $date = new DateTime;
  $date = new DateTime('now');
  
  // Heutiges Datum, Mitternacht
  $date = new DateTime('today');
  
  // Gestern mittag
  $date = new DateTime('yesterday noon');
  
  echo $date->format('d.m.Y H:i:s');
  echo '<br />';
  
  /**
   * Datumsangaben
   */
  
  $birthday = new DateTime('1987-08-16');
  
  echo 'Ich wurde an einem "'.$birthday->format('l').'" geboren. ';
  echo 'War das ein Schaltjahr? '.($birthday->format('L') ? 'Ja!' : 'Nein!');
  echo '<br />';
  
  /**
   * Daten lassen sich sehr einfach modifizieren
   */
  $date->modify('+1 month 3 weeks 4 days 5 hours 23 minutes 15 seconds');
  
  echo $date->format('d.m.Y H:i:s');
  echo '<br />';
  
  /**
   * Daten lassen sich auch vergleichen
   */
  
  $today = new DateTime('today');
  $tomorrow = new DateTime('tomorrow');
  
  if ($today == $tomorrow) {
    echo 'heute ist morgen';
  } else {
    echo 'heute ist nicht morgen';
  }
  
  $today->modify('+1 day');
  
  if ($today == $tomorrow) {
    echo 'heute ist morgen';
  } else {
    echo 'heute ist nicht morgen';
  }