<?

  /**
   * Konstanten sind ähnlich zu Variablen, können aber nur skalare Werte enthalten, welche nicht mehr verändert werden können
   * Skalare Typen sind alle Typen die nicht traversierbar sind (Schleifen nicht möglich)
   * 
   * Konstanten werden in der Regel immer groß geschrieben
   */

  define('FOO', 'bar');
  define('FOO', 'foo');

  /**
   * Man hat die Möglichkeit Konstanten auf Existenz zu prüfen
   */
  
  var_dump(defined('FOO'));
  
  echo FOO;
    
  echo constant('FOO');
  
  /**
   * Konstanten lassen sich im Gegensatz zu Variablen nicht in Strings verwenden und müssen angehängt werden
   */
  
  echo FOO.'bara';
  
  /**
   * Globale Variablen werden entwerder von PHP automatisch gesetzt oder definiert
   * Diese sind von überall aus aufrufbar
   */
  
  // Server
  print_r($_SERVER);
  
  // Beispiel Wordpress
  global $post;