<?

  /**
   * In PHP unterscheidet man verschiedene Typen von Nummern
   */

  // Integer (Ganzzahl)
  $i =  1;
  
  // Float (Gleitkommezahl)
  $j = 1.2;
  
  /**
   * Mit nummerischen Werten kann man rechnen
   * 
   * @see http://php.net/manual/de/ref.math.php
   */
  
  // Einfache Rechnungen
  $x = 1+2+3+4/2+5-1*0;
  
  // Hoch-/Runterzählen
  $i = $i+1;
  $i += 1;
  $i++; // alter Wert wird zurück gegeben
  ++$i; // neuer Wert wird zurück gegeben
  
  // Auf-/Abrunden
  echo round($j); // automatisch
  echo floor($j); // abrunden
  echo ceil($j); // aufgrunden
  
  /**
   * Einfache Strings lassen sich in nummerische Werte umwandeln
   */
  
  $s = '123';
  
  $n = intval($s);
  $n = (int) $s;