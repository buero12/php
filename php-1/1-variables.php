<?

  /**
   * Variablen können in PHP alle möglichen Datentypen enthalten
   */

  $foo = 1337;
  
  /**
   * Variablen können mit beliebigen Typen überschrieben werden und müssen nicht gecastet werden
   */
 
  $foo = 'bar';

  /**
   * Es gibt verschiedene Methoden Variablen auszugeben
   */
  
  echo $foo;
  
  /**
   * In Templates wird oft die Kurzschreibweise verwendet
   */
  
?><?= $foo; ?><?
  
  /**
   * Print unterscheidet sich zu echo nur dadurch, dass es eine Funktion ist und sich somit in Abfragen verwenden lässt
   * 
   * $a === true ? print 'true' : print 'false';
   */
  
  print $foo;
  print($foo);
  
  /**
   * Einfache Anführungszeichen werten Variablen in einem String nicht aus
   */
  
  echo '$foo bar';
  
  /**
   * Doppelte Anführungszeichen lassen sich mit Variablen befüllen, was jedoch langsamer (nicht merkbar) ist als einfacher Anführungszeichen
   */
  
  echo "$foo bar";
  
  /**
   * Um sicher zu gehen, dass eine Variable korrekt erkannt wird, sollte diese in geschwungenen Klammern geschrieben werden
   */
  
  #echo "$foobara"; // Fehler, Variable nicht definiert
  echo "{$foo}bara"; // Ausgabe: "barbara"