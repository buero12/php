<?

  /**
   * Array sind Listen die mit verschiedenen Variablen befülllt werden können
   * Array beginnen an der Position 0
   */

  // Leere Arrays erstellen
  $a = array(); // alte Schreibweise
  $a = []; // neue Schreibweise
  
  // Einfache befüllte Arrays erstellen
  $a = [1, 2, 3, 'foo', 'bar'];
  
  // Elemente hinzufügen
  $a[] = 4; // an das Ende
  array_push($a, 5); // an das Ende
  array_unshift($a, 0); // an den Anfang
  
  // Elemente entfernene
  array_pop($a); // letztes Element entfernen
  array_shift($a); // erstes Element entfernen
  unset($a[2]); // Element an Position 2 entfernen, also das 3 Element
  
  // Teiles eines Arrays entfernen
  $a = array_slice($a, 1, 2);
  
  // Arrays mit Schlüsseln erstellen
  $a = [
    1 => 2,
    'foo' => 'bar'
  ];
  
  // Elemente hinzufügen
  $a['php'] = 'ist super';
  
  // Elemente entfernen
  unset($a['php']);
  
  /**
   * Um zu prüfen, ob es einen Wert in einem Array gibt, gibt es verschiedenen Möglichkeiten
   */
  
  // Prüft auf einen Schlüssel
  var_dump(isset($a['php']));
  var_dump(array_key_exists('php', $a));
  
  // Prüft auf einen Wert
  var_dump(in_array('bar', $a));
  var_dump(array_search('bar', $a));
  
  /**
   * Werte in einem Array können eindeutig gemacht werden
   */
  
  $a = array_unique($a);
  
  /**
   * Werte und Schlüsse lassen sich auf beliebige Arten sortieren
   * 
   * @see http://php.net/manual/de/array.sorting.php
   */
  
  sort($a); // aufsteigend
  rsort($a); // absteigend
  shuffle($a); // zufällig
  /* ... */
  
  /**
   * Es gibt auch die Möglichkeit, sich mit dem Zeiger durch ein Array zu bewegen
   */
  
  // Aktuellen Wert liefern
  echo current($a);
  
  // Aktuellen Schlüssel liefern
  echo key($a);
  
  // Aktuellen Schlüssel und Wert liefern und weiter springen
  print_r(each($a));
  
  // An den Anfang springen
  reset($a);
  
  // An das Ende springen
  end($a);
  
  // Vor
  next($a);
  
  // Zurück
  prev($a);
  
  /**
   * PHP bietet eine Vielzahl von Array-Funktionen
   * 
   * @see http://php.net/manual/de/ref.array.php
   */
  
  // Array zusammenführen
  
  $b1 = [1, 2, 3];
  $b2 = [4, 5, 6];
  
  $b = $b1 + $b2;
  
  // Führt zwei Array zusammen
  $b = array_merge($b1, $b2);
  $b = array_merge_recursive($b1, $b2);
  
  // Ersetzt Schlüssel
  $b = array_replace($b2, $b1);
  $b = array_replace_recursive($b2, $b1);
  
  // Schlüssel und Werte kombinieren
  $b = array_combine($b1, $b2);
  
  /**
   * Es gibt die Möglichkeit, sich alle Werte oder Schlüssel eines Array liefern zu 
   */
  
  $keys = array_keys($a);
  $values = array_values($a);
  
  /**
   * Um alle Inhalte eines Arrays zu bearbeiten, kann man diese mappen
   */
  
  // Alle Elemente beschneiden
  $a = array_map('trim', $a);
  
  // Leere Elemente entfernen
  $a = array_filter($a, 'strlen');