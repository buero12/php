<?

  /**
   * Strings sind Zeichenketten, die alle möglichen Satzzeichen enthalten können
   */

  $foo = 'bar';
  
  /**
   * Strings lassen sich miteinander Kombinieren
   */
  
  $bar = 'bara';
  
  $s = $foo.$bar;
    
  /**
   * Weiterhin stellt PHP viele Funktionen für Strings bereit
   * 
   * @see http://php.net/manual/de/ref.strings.php
   */
  
  // Länge ausgeben
  echo strlen($s);
  
  // Transformieren
  echo strtoupper($s);
  echo strtolower($s);
  echo ucfirst($s);
  echo lcfirst($s);
  
  // Extrahiert einen Teil eines Strings
  echo substr($s, 1, 2);
  
  // String beschneiden (falls nicht anders angegeben, Leer- und Steuerzeichen)
  $s = '  barbara   ';
  echo trim($s);
  echo rtrim($s);
  echo ltrim($s);
  
  // Aufteilen
  $s = '/foo/bar';
  $a = explode('/', $s);
  
  print_r($a);
  
  // Widerholen
  echo str_repeat($s, 2);
  
  $s = 'barbara';
  
  // Teile austauschen
  echo str_replace('bar', 'foo', $s); // Ausgabe: "foofooa"