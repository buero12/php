<?

  /**
   * Manchmal macht es mehr Sinn Inhalte auf das Dateisystem zu schreiben, statt eine Datenbank zu verwenden
   */

  // Datei mit Inhalt erstellen
  file_put_contents('test.txt', 'Test');
  
  // Datei auslesen
  $t = file_get_contents('test.txt');
  
  // Seiten auslesen
  echo file_get_contents('http://buero12.com/');