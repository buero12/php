<?

  /**
   * Mithilfe dieser Methoden lassen sich auch entfernte Dateien bzw. URLs auslesen
   * Wir sprechen dann von sogenanten APIs
   */
    
  $url = "https://de.wikipedia.org/w/api.php?format=json&action=query&prop=extracts&exintro=&explaintext=&titles=Pforzheim";
  
  $response = file_get_contents($url);
  
  echo $response;
  
  /**
   * APIs liefern Daten of in maschinenlesbare Formaten wie XML, JSON oder Serialized aus
   * Diese Daten müssen dann erst wieder dekodiert werden
   */
  $data = json_decode($response);
  
  print '<pre>';
  print_r($data);
  print '</pre>';