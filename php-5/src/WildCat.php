<?

  class WildCat extends Cat
  {
    
    public function play()
    {
      
      echo '... you lost your arm while trying to playing with '.$this->name.' ...';
      
      return $this;
      
    }
    
    public static function getSound()
    {
      return '... ROAR ...';
    }
    
  }