<?

  class Cat
  {
    
    // Public sind von überall aus verfügbar
    public $name;
    
    // Protected ist nur in dieser Klasse sowie in verwandten Klassen verfügbar
    protected $hunger = 50;
    protected $tiredness = 50;
    protected $fitness = 50;
        
    // Private ist nur in dieser Klasse verfügbar
    private $dead = false;
    
    // Konstruktor erstellt ein neues Objekt
    public function __construct($name)
    {
      $this->name = $name;
    }
    
    // Normalisiert die Werte der Katze
    private function normalize()
    {
      
      $this->fitness = min($this->fitness, 100);
      
      if ($this->fitness <= 0) {
        $this->dead = true;
      }
      
      $this->hunger = min(0, $this->hunger);
      
      if ($this->hunger >= 100) {
        $this->dead = true;
      }
      
      $this->hunger = min(0, $this->hunger);
      
      if ($this->tiredness >= 100) {
        $this->dead = true;
      }
      
    }
    
    public function sleep()
    {
      
      $this->tiredness -= 10;
      $this->hunger += 5;
      
      $this->normalize();
      
      echo '... brrr ...';
      
      return $this;
      
    }
    
    public function getIsTired()
    {
      return $this->tiredness >= 70;
    }
    
    public function eat()
    {
      
      $this->hunger -= 10;
      $this->tiredness += 5;
      
      $this->normalize();
      
      echo '... nom ...';
      
      return $this;
      
    }
    
    public function getIsHungry()
    {
      return $this->hunger >= 70;
    }
    
    public function play()
    {
      
      $this->fitness += 10;
      $this->hunger += 5;
      $this->tiredness += 5;
      
      $this->normalize();
      
      echo self::getSound();
      
      return $this;
      
    }
    
    public function getIsLazy()
    {
      return $this->fitness <= 30;
    }
    
    // Gibt das Befinden der Katze aus
    public function __toString()
    {
            
      $status = [];
      
      if ($this->dead === false) {

        if ($this->getIsTired()) {
          $status[] = 'tired';
        }

        if ($this->getIsHungry()) {
          $status[] = 'hungry';
        }

        if ($this->getIsLazy()) {
          $status[] = 'lazy';
        }

        if (empty($status)) {
          $status[] = 'fine :)';
        }
      
      } else {
        $status[] = 'dead :(';
      }
      
      return '<br />'.$this->name.' is '.implode(', ', $status).'<br />';
      
    }
    
    // Statische Funktionen können aufgerufen werden, ohne ein Objekt zu erstellen
    public static function getSound()
    {
      return '... miau ...';
    }
    
  }
  