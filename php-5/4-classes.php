<?

  /**
   * Klassen sind Schablonen für Objekte und können neben Variablen auch Funktionen enthalten
   * Variablen und Funktionen können dabei unterschiedliche Sichtbarkeiten haben
   */

  require_once('src/Cat.php');
  
  echo 'Die Katzen macht '.Cat::getSound().'!<br />';

  $mimi = new Cat('Mimi');
  
  echo $mimi;
  
  $mimi->play()->play()->play()->play();
  
  echo $mimi;
  
  $mimi->eat()->eat();
  
  echo $mimi;
  
  $mimi->sleep()->sleep();
  
  echo $mimi;
  
  $mimi->play()->play()->play()->play()->play()->play()->play()->play();
  
  echo $mimi;
  
  echo '<hr />';
  
  /**
   * Klassen können abgeleitet werden
   */
  
  require_once('src/WildCat.php');
  
  echo 'Der Tiger macht '.WildCat::getSound().'!<br />';
  
  $tiger = new WildCat('Tiger');
  
  $tiger->play();