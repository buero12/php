<?

  /**
   * Namespaces sind seit PHP 5.3 verfügbar und dienen Dazu, Namenskonflikte aufzulösen
   */

  include 'src/Image.php';
  include 'src/ImageAlt.php';
  
  /**
   * Beide Klassen haben den Namen 'Image', werden aber über den Namespace unterschieden
   */
  
  $image = new \Foo\Image;
  $imageAlt = new \Bar\Image;