<?

  /**
   * Objekte gehört seit PHP 5 zum Standard
   * Objekte sind in erster Linie ähnlich zu Arrays
   */

  function getName($o)
  {
    return $o->first_name.' '.$o->last_name;
  }

  $o = new stdClass();
  $o->first_name = 'Lukas';
  $o->last_name = 'Rydygel';
  
  var_dump(getName($o));