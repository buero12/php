<?

  /**
   * Funktionen sind eines der grundlegenden Elemente in PHP
   * Funktionen sollte immer eingesetzt werden, um redundanten Code zu vermeiden
   */

  // Prüfe ob ein Wert zwischen zwei Werten liegt
  function between($x, $min, $max)
  {
    return $x > $min && $x < $max;
  }
  
  // Kann von überall aus aufgerufen werden
  var_dump(between(5, 0, 10));
  
  // Parameter können Optional sein oder müssen bestimmte Kriterien erfüllen
  function myImplode(array $a, $glue = ', ', $last = null)
  {
    
    $end = null;
    
    if (!is_null($last)) {
      $end = array_pop($a);
    }
    
    return implode($glue, $a).$last.$end;
    
  }
  
  $a = [
    'männlich', '184 groß', 'ein Nerd'
  ];
  
  $desc = myImplode($a, ', ', ' und ');
  
  echo "Ich bin {$desc}!";
  
  /**
   * Funktionen können sich rekursiv aufrufen
   */
  
  $a = [2, 3, 1, 6, 0, 9, 7, 5, 8, 4];

  function mySort(array $a, array $result = [])
  {
    
    // Wenn $a leer ist, gib das Ergebnis zurück
    if (empty($a)) {
      return $result;
    }
    
    // Hole das letzte Element aus $a
    // $a wird dadurch kürzer
    $x = array_pop($a);
    
    $added = false;

    // Durchlaufe das Ergebnis
    for ($i = 0; $i < count($result); $i++) {

      // Ist der Wert kleiner als das Ergebnis an Position $i?
      if ($x < $result[$i]) {
        
        // Schiebe den Wert zwischen ein
        array_splice($result, $i, 0, $x);
        $added = true;
        break; // Verlasse die Schleife
        
      }

    }
    
    // Wurde Wert nicht eingefügt
    // Hänge ihn an das Ende
    if ($added === false) {
      $result[] = $x;
    }
        
    // Rekursiver Funktionsaufruf
    return mySort($a, $result);
    
  }
  
  print_r(mySort($a));