<?

  /**
   * Um eine Datei schlanker zu machen, kann man Teile davon auslagern und inkludieren
   */

  // Ein include wirft eine Warnung, wenn die Datei nicht existiert und läuft weiter
  include 'src/Cat.php';
  #include('src/Cat.php');
  
  // Ein require wirft einen Fehler, wenn die Datei nicht existiert und bricht ab
  #require 'src/Cat.php';
  #require('src/Cat.php');
  
  // Das Schlüsselwort once gibt an, dass die Datei nur einmal geladen werden soll
  #include_once 'src/Cat.php';
  #include_once('src/Cat.php');
  #require_once 'src/Cat.php';
  #require_once('src/Cat.php');