<?

  /**
   * Anfragen stellen einen wichtigen Bestandteil für die Interaktion der Nutzer mit der Seite dar
   */

  /**
   * Anfragen werden zum einen über Pfade gestellt
   * 
   * /index.php
   * /contact.php
   * /shop.php
   */

  /**
   * Es können unterschiedliche Anfragen auf dem gleichen Pfad liegen (REST)
   * 
   * In der Regel arbeitet man im Browserumfeld allerdings nur mit Parametern
   * 
   * /shop.php?add_to_cart=1
   */

   /**
    * Neben den GET-Parametern können auch POST-Parameter eingesetzt werden
    * Hierzu benötigt man allerdings Formulare
    */