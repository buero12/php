<?

  /**
   * GET und POST-Parameter können über die globale Variable $_GET bzw. $_POST abgefragt werden
   */

   echo 'Hallo '.$_GET['hallo'].'!';
   echo 'Hallo '.$_POST['hallo'].'!';
   
   // Auf Parameter kann man prüfen
   var_dump(isset($_GET['hallo']));
   var_dump(array_key_exists('hallo', $_GET));
   
   var_dump(isset($_POST['hallo']));
   var_dump(array_key_exists('hallo', $_POST));
   
   // In einem Parameter wird immer ein String übergeben
   $id = intval($_GET['id']);
   $id = intval($_POST['id']);