<?

  /**
   * Wenn man eine Anzahl an Möglichkeiten für eine Bedingung kennt, ist es manchmal Ratsam kein if/else zu verwenden
   */

  $code = 200;
  
  switch ($code) {
    
    case 200:
      echo 'OK';
    break;
  
    case 404:
      echo 'NOT FOUND';
    break;
  
    default:
      echo 'ERROR';
    
  }
  
  // Templates
  
  switch ($code) :
    
    case 200:
      echo 'OK';
    break;
  
    case 404:
      echo 'NOT FOUND';
    break;
  
    default:
      echo 'ERROR';
    
  endswitch;