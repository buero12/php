<?

  /**
   * Reguläre Ausdrücke dienen dazu, Eingaben zu identifizieren und auszuwerten
   */

  // Strings finden und ersetzen
  $s = 'Ich finde Mäuse süß!';
  echo preg_replace('/Mäuse/', 'Katzen', $s);
  
  // Ausdrücke finden und ersetzen
  $s = 'Ich habe 0€ im Geldbeutel.';
  echo preg_replace('/(\d+)/', 100, $s);
  
  // Auf die Struktur einer E-Mail Adresse prüfen
  function validateMail($mail)
  {
    return preg_match('/[a-z0-9._-]+@[a-z0-9._-]+\.[a-z]{2,3}$/is', $mail);
  }
  
  var_dump(validateMail('lukas.rydygel@info-pforzheim.de'));
  var_dump(validateMail('lukas.rydygel@info-pforzheim'));