<?

  /**
   * Eine fore-Schleife ist eine einfache Art sich durch ein Array zu bewegen
   * Sie wird solange ausgeführt, bis der Zählern einen bestimmten Wert erreicht
   */

  $a = [
    'the', 'quick', 'brown', 'fox', 'jumps', 'over', 'the', 'lazy', 'dog'
  ];
  
  for ($i = 0; $i < count($a); $i++) {
    echo $a[$i].' ';
  }
  
  // Templates
  
  for ($i = 0; $i < count($a); $i++) :
    echo $a[$i].' ';
  endfor;
  
  /**
   * Es gibt die Möglichkeit das Verhalten in Schleifen zu steuern
   */
  
  $a = [1, 2, 3, 4, 5, 6, 7, 8, 9];
  
  // Schleife weiterführen und Element überspringen wenn eine Bedingung zutrifft
  for ($i = 0; $i < count($a); $i++) {
    
    if ($a[$i]%2 === 0) {
      continue;
    }
    
    echo $a[$i].' ';
    
  }
  
  // Schleife verlassen wenn eine Bedingung zutrifft
  for ($i = 0; $i < count($a); $i++) {
    
    if ($a[$i] > 5) {
      break;
    }
    
    echo $a[$i].' ';
    
  }