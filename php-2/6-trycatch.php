<?

  /**
   * Try/Catch Anweisungen dienen dazu, Fehler abzufangen und entsprechende Meldungen anzuzeigen
   */

  try {
    
    $x = 100;
    $y = 0;
    
    if ($y === 0) {
      throw new Exception('Division durch Null.');
    }
    
    echo $x/$y;
    
  } catch (Exception $e) {

    echo 'Sorry: '.$e->getMessage().' Bist du dumm?';
    
  }