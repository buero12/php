<?

  /**
   * Um dynamische Webseiten zu erstellen, ist es notwendig, dass man mit Bedingung arbeitet
   */

  $t = true;
  
  // Einfache if/else Anweisung
  if ($t) {
    echo 'true';
  } else {
    echo 'false';
  }
  
  // Strikte if/else Anweisung
  if ($t === true) {
    echo 'true';
  } elseif ($t === false) {
    echo 'false';
  }
  
  // Klammern müssen nicht geschrieben werden, wenn nach der Bedingung nur eine Zeile folgt
  // Dies ist jedoch nicht ratsam, da sich der Code verändern kann und auch Leerzeilen berücksichtigt werden
  
  if ($t)
    echo 'true';
  else
    echo 'false';
  
  // Einzeiler sind bequem
  echo $t ? 'true' : 'false';
  $t ? print('true') : print('false');
  
  /**
   * Bedingungen lassen sich beliebig of weiterführen und verschachteln
   * Guter Code hat jedoch möglichst wenig verschachtelte Blöcke
   */
  
  $cats = 1;
  
  // funktioniert, ist aber schlecht
  if ($cats === 0) {
    echo 'keine Katzen';
  } else {
    
    if ($cats === 1) {
      echo 'eine Katze';
    } else {
      
      if ($cats > 17) {
        echo 'zu viele Katzen';
      } else {
        echo "{$cats} Katzen";
      }
      
    }
    
  }
  
  // funktioniert genauso gut und ist besser
  if ($cats === 0) {
    echo 'keine Katzen';
  } elseif ($cats === 1) {
    echo 'eine Katze';
  } elseif ($cats > 17) {
    echo 'zu viele Katzen';
  } else {
    echo "{$cats} Katzen";
  }
  
  /**
   * Es lassen sich auch mehrere Bedigungen durchlaufen
   */
  
  $gender = 'male';
  
  if ($cats === 1 && $gender = 'male') {
    echo 'ein Kater';
  }
  
  /**
   * In Templates ist es oft hilfreich, eine andere Schhreibweise zu verwenden, da man besser erkennen kann, wo welcher Befehlt aufhört
   */
  
  if ($t === true) :
    echo 'true';
  elseif ($t === false) :
    echo 'false';
  endif;