<?

  /**
   * Eine foreach-Schleife ist die einfachste Möglichkeit ein Array zu durchlaufen
   * Sie wird solange ausgeführt, bis das Ende eines Arrays erreicht ist
   */

  $a = [
    'the', 'quick', 'brown', 'fox', 'jumps', 'over', 'the', 'lazy', 'dog'
  ];
  
  foreach ($a as $value) {
    echo $value.' ';
  }
  
  // Templates
  
  foreach ($a as $value) :
    echo $value.' ';
  endforeach;
  
  /**
   * Man kann auch auf die Schlüssel eines Arrays zurückgreifen
   */
  
  $a = [
    'Vorname' => 'Lukas',
    'Nachname' => 'Rydygel'
  ];
  
  foreach ($a as $key => $value) {
    echo "{$key}: {$value}<br />";
  }
  
  /**
   * Um die Werte eines Array zu bearbeiten gibt es mehrere Möglichkeiten
   */
  
  $a = [1, 2, 3, 4, 5];
  
  foreach ($a as $key => $value) {
    $a[$key] = $value*2;
  }
  
  print_r($a);
  
  foreach ($a as &$value) {
    $value *= 2;
  }
  
  print_r($a);