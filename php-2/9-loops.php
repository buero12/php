<?

  /**
   * Wann verwende ich welche Schleife?
   */

  // Bei gewöhnlichen Zählern, sollte man eine For-Schleife verwenden
  for ($i = 0; $i < 10; $i++) {
    echo $i; // Ausgabe 123456789
  }
  
  $a = ['php', 'html', 'css', 'javascript', 'java', 'ruby'];

  // Wenn man ein Array durchläuft, ist eine For-Each-Schleife immer eine gute Wahl, beonders dann, wenn auch den Schlüssel benötigt (assoziatives Array)
  foreach ($a as $i) {
    echo $i.', '; // Ausgabe php, html, css, javascript, java, ruby,
  }
  
  // Im Gegensatz zu einer For-Schleife, muss man sich nicht um den Zähler und die Bedingung kümmern/
  for ($i = 0; $i < count($a); $i++) {
    echo $a[$i].', '; // Ausgabe php, html, css, javascript, java, ruby,
  }
  
  /**
   * Unterschied While und Do-While
   */
    
  $i = 0;

  // Bedingung wird bereits am Anfang geprüft, somit sind auch keine Durchläufe möglich
  while ($i > 0) {
    echo $i; // keine Ausgabe
  }
    
  // Bedingung wird am Ende geprüft, somit ist immer ein Durchlauf sicher
  do {
    echo $i; // Ausgabe 0
  } while ($i > 0);