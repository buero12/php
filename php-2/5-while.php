<?

  /**
   * While-Schleifen unterscheiden sich dahingehend, dass sie so lange ausgeführt werden, bis eine Bedingung nicht mehr zutrifft
   */
  
  $a = [
    'the', 'quick', 'brown', 'fox', 'jumps', 'over', 'the', 'lazy', 'dog'
  ];
  
  while (!empty($a)) {
    echo array_shift($a).' ';
  }
  
  // Templates
  while (!empty($a)) :
    echo array_shift($a).' ';
  endwhile;
  
  $empty = false;
  
  while ($empty === false) {
    
    echo array_shift($a).' ';
    
    if (empty($a)) {
      $empty = true;
    }
    
  }
  
  // Endlosschleifen, wenn die Bedingung sich nie ändert
  #while (true) { ... }