<?

  /**
   * Die Hausaufgabe besteht darin, ein einfaches Schere-Stein-Papier zu programmieren
   */

  /**
   * Multipliziert alle Werte von zwei Arrays
   * 
   * $a1 = [85, 29, '65', 10, '39', 95, 2, 45]
   * $a2 = [20, '97', '2', 9, 88, 23, 0]
   */

  /**
   * Berechnet die Fibonacci-Folge bis zum 50 Wert
   */

  /**
   * Berechnet, wie viele Männer und Frauen es gibt
   * Berechnet das Gesamtalter aller Personen
   * Berechnet das Gesamtalter der beiden Geschlechtergruppen
   * Bestimmte die älteste und jünste Person bzw. Personen
   * 
   * $a = [
   *  [
   *    'first_name' => 'Lukas',
   *    'last_name' => 'Rydygel',
   *    'gender' => 'male',
   *    'age' => 29
   *  ], [
   *    'first_name' => 'Markus',
   *    'last_name' => 'Arnold',
   *    'gender' => 'male',
   *    'age' => 34
   *  ], [
   *    'first_name' => 'Michelle',
   *    'last_name' => 'Heintz',
   *    'gender' => 'female',
   *    'age' => 21
   *  ], [
   *    'first_name' => 'Sarah',
   *    'last_name' => 'Godel',
   *    'gender' => 'female',
   *    'age' => 21
   *  ], [
   *    'first_name' => 'Sabrina',
   *    'last_name' => 'Koch',
   *    'gender' => 'female',
   *    'age' => 24
   *  ], [
   *    'first_name' => 'Frank',
   *    'last_name' => 'Bässler',
   *    'gender' => 'male',
   *    'age' => 41
   *  ], [
   *    'first_name' => 'Mirjam',
   *    'last_name' => 'Müller',
   *    'gender' => 'female',
   *    'age' => 44
   *  ]
   * ];
   */


  /**
   * Ein Geldautomat soll möglichst wenig Rückgeld (Münzen) ausgeben
   */

  /**
   * Verschiedenen Rollenspielfiguren sollen gegeneinander antreten
   * Der Charakter mit der höheren Punktezahl gewinnt den Kampf
   * 
   * Gesamtpunkte berechnen und dann vergleichen:
   * Health * 1
   * Attack * 2.5
   * Defence * 2
   * Agility * 1.5
   */
   $a = [
     [
       'name' => 'Atilla der Hunnenkönig',
       'health' => 70/100,
       'attack' => 80/100,
       'defence' => 60/100,
       'agility' => 40/100
     ], [
       'name' => 'Lrrr, Herrscher über den Planeten Omicron Persei 8',
       'health' => 80/100,
       'attack' => 90/100,
       'defence' => 50/100,
       'agility' => 20/100
     ], [
       'name' => 'XXX der Unbekannte Ninja',
       'health' => 70/100,
       'attack' => 60/100,
       'defence' => 60/100,
       'agility' => 80/100
     ], [
       'name' => 'Das fliegende Spaghetti-Monster',
       'health' => 40/100,
       'attack' => 30/100,
       'defence' => 30/100,
       'agility' => 50/100
     ], [
       'name' => 'Das unsichtbare rosafarbene Einhorn',
       'health' => 50/100,
       'attack' => 30/100,
       'defence' => 30/100,
       'agility' => 50/100
     ], [
       'name' => 'Conen der Barbar',
       'health' => 80/100,
       'attack' => 70/100,
       'defence' => 60/100,
       'agility' => 50/100
     ], [
       'name' => 'Xena – Die Kriegerprinzessin',
       'health' => 60/100,
       'attack' => 60/100,
       'defence' => 70/100,
       'agility' => 70/100
     ], [
       'name' => 'Santa Klaus',
       'health' => 100/100,
       'attack' => 40/100,
       'defence' => 40/100,
       'agility' => 10/100
     ],
   ];
   
   /**
    * PHP gibt Wochentage in der Regel in Englisch aus, wir möchte diese aber in Deutsch haben
    * 
    * 1. Lösungsweg sind Schleifen
    * 2. Lösungsweg ist If/Else
    * 3. Lösungsweg ist Switch/Case
    * 
    * Weiterhin soll bestimmt werden, ob gerade Wochenende ist und ob wir in die Kirche gehen
    */